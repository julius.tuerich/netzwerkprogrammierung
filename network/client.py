import socket


class Client(object):
    def send(self, host, port, message):
        """
        Sends a message to a client.

        :param host:
        :param port:
        :param message:
        :return:
        """
        # create client socket
        client_socket = socket.socket()

        # connect to server
        client_socket.connect((host, port))

        # send message
        client_socket.send(message.encode())

        # receive response (up to 10kb)
        data = client_socket.recv(1024 * 10).decode()

        # close the connection
        client_socket.close()

        return str(data)
