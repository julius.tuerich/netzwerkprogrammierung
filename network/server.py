import socket
import subprocess
import yaml
import time
from _thread import *
from datetime import datetime
from network.server_node import ServerNode


class Server(object):

    def __init__(self, host, port, config_file, proxy_to_master = True):
        """
        Creates a new Server object.

        :param host:
        :param port:
        :param config_file:
        """
        self.host = host
        self.port = port
        self.config_file = config_file
        self.proxy_to_master = proxy_to_master
        self.running = True
        self.master = None
        self.server_nodes = self.get_server_nodes()

    def shut_down(self):
        """
        Shuts down the server.

        :return:
        """
        self.running = False

    def listen(self):
        """
        Binds to the port and accepts connections.

        :return:
        """
        # log info about proxy
        self.log("INFO: The proxying of commands to the master server is " +
                 ("ENABLED" if self.proxy_to_master else "DISABLED"))

        # create the socket
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)

        # bind the socket to host and port
        server_socket.bind((self.host, self.port))

        # log status
        self.log("SUCCESS: Bound socket successfully to " + self.host + " on " + str(self.port))

        # listen for connections
        server_socket.listen(socket.SOMAXCONN)

        # start the master service in the background which is responsible for deciding which server node / instance
        # is the current master and gets the commands.
        start_new_thread(self.master_service, ())

        # check for new connections and handle commands until server is shut down
        while self.running:
            connection, client_address = server_socket.accept()

            # receive up to 10kb of data
            data = connection.recv(1024 * 10).decode()
            if not data:
                # if data is not available break
                break

            # log status (if not ping)
            if data != "server:ping":
                self.log("received: " + str(data))

            # handle data
            result = self.handle(client_address, str(data))
            connection.send(result.encode())

            # close connection
            connection.close()

        server_socket.close()

    def handle(self, client_address, data):
        """
        Handles an incoming message and returns data to be sent.

        :param data:
        :return:
        """

        config = self.load_config()

        # check for status command
        if data == "server:list":
            command_list = " - server:exit\n - server:info\n - server:list\n - server:master\n - server:status \n - " \
                           "server:who-is-master"
            for job in config["jobs"]:
                command_list += "\n - job:" + job["name"]
            return "The following commands are available:\n" + command_list

        # check for status command
        if data == "server:status":
            return "OK! 200"

        # check for status command
        if data == "server:ping":
            return "Ping OK!"

        # check for status command
        if data == "server:info":
            return "TCP server running at " + self.host + " on " + str(self.port)

        # check for status command
        if data == "server:master":
            is_self = lambda node: node.host == self.host and node.port == self.port
            return "True" if is_self(self.master) else "False"

        # check for status command
        if data == "server:who-is-master":
            return "The server [" + self.master.name + "] at " + self.master.host + " on " + str(self.master.port) \
                   + " is the master"

        # check for exit command
        if data == "server:exit":
            self.shut_down()
            self.log("Server was shut down by " + str(client_address))
            return "SUCCESS!"

        # check for master command
        # if data == "server:master":
        #   ...

        # check if the this server instance is not the current master and forward the job
        is_self = lambda node: node.host == self.host and node.port == self.port
        if self.proxy_to_master and not is_self(self.master):
            print()
            return "[Note: Input was proxied to master server [" + self.master.name + "] at " + self.master.host + \
                   " on " + str(self.master.port) + "]\n" + self.master.send(data)

        # this instance is the master, so we check for configurable jobs from yaml file and execute if found
        for job in config["jobs"]:
            name = job["name"]
            script = job["script"]
            if data == "job:" + name:
                result = subprocess.run(script, shell=True, capture_output=True, text=True)
                self.log("Running job [" + name + "]")
                return result.stdout

        return "Unknown command [" + data + "]"

    def master_service(self):
        """
        This function runs constantly in the background and determines the master of the network (connected servers).
        The master is responsible for handling the commands.

        :return:
        """
        while self.running:
            # refresh server nodes from config file
            self.server_nodes = self.get_server_nodes()

            # lambda do determine if node is the current server instance
            is_self = lambda node: node.host == self.host and node.port == self.port

            # filter active server nodes and sort them by host (ip address) and port
            active_nodes = [node for node in self.server_nodes if is_self(node) or node.is_alive()]
            active_nodes.sort(key=lambda node: node.index())

            # mark master status
            master_node = active_nodes[0]
            is_master = is_self(master_node)

            # check if master changed and notify in log about master change
            if self.master != master_node:
                self.log("STATUS: " + ("I am the new master [" + master_node.name + "]"
                                       if is_master else "[" + master_node.name + "] is the new master"))

                # update the master status
                self.master = master_node

            # wait for one second to spare resources
            time.sleep(2)

    def get_server_nodes(self):
        """
        Returns a list of server nodes (potential connected servers) in the network.

        :return:
        """
        config = self.load_config()

        server_nodes = []
        for server in config["servers"]:
            server_nodes.append(ServerNode(server["name"], server["host"], server["port"]))

        return server_nodes

    def log(self, msg):
        """
        Logs a message with the current timestamp to the console.

        :param msg:
        :return:
        """
        timestamp = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        print("[" + timestamp + "] ", msg)

    def load_config(self):
        """
        Loads the configuration from the yaml file.

        :return:
        """
        with open(self.config_file, "r") as stream:
            return yaml.safe_load(stream)
