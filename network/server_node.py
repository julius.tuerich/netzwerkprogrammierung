import socket


class ServerNode(object):
    def __init__(self, name, host, port):
        self.name = name
        self.host = host
        self.port = port

    def index(self):
        """
        Returns a unique index number for sorting purposes.

        :return:
        """
        ip_parts = list(map(int, self.host.split('.')))
        ip_number = (16777216 * ip_parts[0]) + (65536 * ip_parts[1]) + (256 * ip_parts[2]) + ip_parts[3]

        concatenated_number = int(str(ip_number) + str(self.port))
        return concatenated_number

    def is_alive(self):
        """
        Returns if a server instance (represented by this node) is still alive.

        :return:
        """
        try:
            # create the socket to the server
            client_socket = socket.socket()

            # connect to server
            result = client_socket.connect_ex((self.host, self.port))

            # the server is alive
            if result == 0:
                client_socket.send("server:ping".encode())
                client_socket.close()
                return True
            # the server is not available
            else:
                client_socket.close()
                return False
            # Close the socket
        except:
            return False

    def send(self, command):
        """
        Sends a command to the server instance.

        :param command:
        :return:
        """
        # create the socket to the server
        client_socket = socket.socket()

        # connect to server
        client_socket.connect((self.host, self.port))

        # send command to server
        client_socket.send(command.encode())

        # receive response (up to 10kb)
        data = client_socket.recv(1024 * 10).decode()

        client_socket.close()

        # return response
        return data

    def __eq__(self, other):
        """
        Returns if two ServerNode objects are the same, which is by definition
        if the two objects have the same host and port.

        :param other:
        :return:
        """

        return isinstance(other, ServerNode) and self.host == other.host and self.port == other.port
