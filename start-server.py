import sys
from network.server import Server


def parse_arg(arg):
    """
    Returns the formatted and parsed argument.
    :param arg:
    :return:
    """
    return arg[arg.find("=") + 1:]


if __name__ == "__main__":
    if len(sys.argv) < 4 or len(sys.argv) > 5:
        print("Invalid number of arguments! Use script e.g. `python3 start-server.py --host=127.0.0.1 --port=5001 "
              "--config=server.config.yaml [--disable-proxy]`")
        exit(1)

    # get configuration from argv
    if len(sys.argv) == 4:
        [_, host, port, config_file] = sys.argv
        proxy_to_master = True
    else:
        [_, host, port, config_file, _] = sys.argv
        proxy_to_master = not sys.argv[4] == "--disable-proxy"

    host = parse_arg(host)
    port = int(parse_arg(port))
    cofig_file = parse_arg(config_file)

    # start server
    server = Server(host, port, cofig_file, proxy_to_master)
    server.listen()