import sys
from network.client import Client


def parse_arg(arg):
    """
    Returns the formatted and parsed argument.
    :param arg:
    :return:
    """
    return arg[arg.find("=") + 1:]


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Invalid number of arguments! Use script e.g. `python3 start-client.py --host=127.0.0.1 --port=5001")
        exit(1)

    # get configuration from argv
    [_, host, port] = sys.argv
    host = parse_arg(host)
    port = int(parse_arg(port))

    # start server
    client = Client()

    # read input
    print("Ready to send commands to", host, "on", port, "| Enter `exit` to abort.")
    message = input("=> ")

    while message.lower().strip() != 'exit':
        # send data
        response = client.send(host, port, message)

        print(response)

        # read input again
        message = input("=> ")
