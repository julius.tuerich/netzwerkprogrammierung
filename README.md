# Netzwerkprogrammierung Abschlussprojekt

In diesem Repo befindet sich die Ausarbeitung des Abschlussprojekts des Moduls Netzwerkprogrammierung. 

## Abhängigkeiten
Die einzige externe Abhängigkeit dieses Projekts ist das Package `pyyaml`, welches beim Einlesen der 
YAML-Konfigurations-Datei behilflich ist und über die `requirements.txt` installiert werden kann:
```
pip3 install -r requirements.txt 
```

## Einleitung
Das Projekt besteht im Wesentlichen aus TCP-Servern welche mithilfe einer Python-Klasse in anderen Skripten, 
oder direkt über die Kommandozeile mithilfe des Skripts `start-server.py` gestartet werden können.
Den Servern liegt eine Konfigurationsdatei `server.config.yaml` zugrunde, welche eine Liste aller verfügbaren Server 
inkl. Name, Host und Port enthält, damit alle Server wissen, welche Server sich potentiell in dem Netzwerk-Cluster 
befinden können.

Zusätzlich wird noch ein Skript `start-client.py` zur Verfügung gestellt, welches Kommandos an einen Server senden kann,
um zu testen, wie dieser sich verhält.

Innerhalb der Server stehen verschiedene Kommandos zur Verfügung, welche Informationen und Funktionen dieses jeweiligen 
Server zur Verfügung stellen können - diese beginnen mit dem Prefix `server:`.
In der Konfigurationsdatei `server.config.yaml` können ebenfalls weitere sogenannte "Jobs" ergänzt werden, welche als 
Kommando über den Prefix `job:` ausgeführt werden können.

Die Server ermitteln selbstständig den jeweils zuständigen Masterserver. Hierbei ist die Niedrigste IP-Adresse (Host) 
und als zweites Kriterium der niedrigste Port ausschlaggebend. 
Jobs werden immer auf dem Masterserver ausgeführt (es sei denn dies wird unterdrückt) - das bedeutet, dass ein 
Nicht-Masterserver, welcher ein Job-Kommando erhält, hier wie ein Proxy agiert und dieses an den Masterserver übergibt.

## Die Konfigurationsdatei

In der YAML-Datei `server.config.yaml` kann konfiguriert werden, wie viele Server in dem Netzwerk vorhanden sind.
Beispielhaft sind hier die folgenden drei Server bereits zum Testen hinterlegt - können aber nach Belieben angepasst werden:

```yaml
servers:
  # Erster Server
  - name: Server-One
    host: 127.0.0.1
    port: 5001
    
  # Zweiter Server
  - name: Server-Two
    host: 127.0.0.1
    port: 5002
    
  # Dritter Server
  - name: Server-Three
    host: 127.0.0.1
    port: 5003
```

Weiter unten in der Datei können die Jobs eingerichtet werden, welche von dem Server ausführbar sein sollen.
Diese können auch zur Laufzeit des Servers über die Datei angepasst werden. Beispielhaft hinterlegt sind hier ein Job, 
welcher eine Shell-Datei ausführt, um die aktuelle Uhrzeit zu erhalten und einer, welcher ein Python-Skript ausführt, 
um den aktuellen Tag zu erhalten. Auch diese können beliebig ergänzt werden.

```yaml
jobs:
  # Erster Job: Shell-Skript-Aufruf, um die aktuelle Uhrzeit zu erhalten
  - name: get-time
    script: sh ./jobs/get-time.sh
    
  # Zweiter Job: Python-Skript-Aufruf, um das aktuelle Datum zu erhalten
  - name: get-date
    script: python3 ./jobs/get-date.py
```

## Starten der Server
In diesem Beispiel starten wir die Server über die Kommandozeile mithilfe des Skripts `start-server.py`. 
Der Befehl, um dieses zu starten sieht abstrakt wie folgt aus:
```shell
python3 start-server.py --host=YOUR_HOST --port=YOUR_PORT --config=CONFIG_FILE [--disable-proxy]
```

**Als Beispiel starten wir hier also unsere drei Server (wie in der Konfigurationsdatei angegeben) in jeweils einzelnen 
Prozessen bzw. Fenstern aus dem Terminal (in umgekehrter Reihenfolge):**

* Dritter Server (Port=5003)
```shell
python3 start-server.py --host=127.0.0.1 --port=5003 --config=server.config.yaml
```
* Zweiter Server (Port=5002)
```shell
python3 start-server.py --host=127.0.0.1 --port=5002 --config=server.config.yaml
```
* Erster Server (Port=5001)
```shell
python3 start-server.py --host=127.0.0.1 --port=5001 --config=server.config.yaml
```

Man kann nun schon auf den Ausgabe-Streams der Prozesse beobachten, wie beim Starten eines neuen Servers, der Master wechselt, da der Port 
bei dem jeweils jüngeren Server bei gleicher IP-Adresse immer niedriger wird.
Die Server können nach Belieben beendet oder später auch wieder neu gestartet werden.

## Interaktion über Client
Um die Server zu testen, kann mithilfe des Skripts `start-client.py` ein einfacher Client gestartet werden, welcher 
sich mit einem Server verbindet und Kommandos sendet.

Starten wir so z.B. einen Client zu unserem 2. Server:

```shell
python3 start-client.py --host=127.0.0.1 --port=5002
```

Nun können wir Kommandos an den Server senden, wie man in dieser Ausgabe erkennen kann:

```
Ready to send commands to 127.0.0.1 on 5002 | Enter `exit` to abort.
=> server:status
OK! 200
=> server:info
TCP server running at 127.0.0.1 on 5003
=> server:master
False
=> server:who-is-master
The server [Server-One] at 127.0.0.1 on 5001 is the master
=> job:get-time
[Note: Input was proxied to master server [Server-One] at 127.0.0.1 on 5001]
19:14:01

=> 
```

Eine vollständige Liste der verfügbaren Kommandos (inkl. konfigurtierter Job-Kommandos) kann über `server:list` 
abgefragt werden. So lautet diese in unserem Beispiel mit der Standard-Konfiguration:
```
=> server:list
The following commands are available:
 - server:exit
 - server:info
 - server:list
 - server:master
 - server:status 
 - server:who-is-master
 - job:get-time
 - job:get-date
```

Erklärung:
- `server:exit` beendet einen Server und schließt die Socket
- `server:info` gibt eine Info über Host und Port des TCP-Servers aus
- `server:list` gibt alle verfügbaren Kommandos auf dem Server zurück
- `server:master` gibt an, ob dieser Server der Master-Server ist
- `server:status` gibt an, ob die Socket-Verbindung aktiv ist
- `server:who-is-master` gibt zurück, welcher Server aktuell der Master-Server ist (inkl. Host und Port)
- `job:get-time` ist ein eigenes Kommando, welches über die Konfigurationsdatei bereitgstellt wurde und die aktuelle Uhrzeit zurückgibt
- `job:get-date` ist ein eigenes Kommando, welches ebenfalls über die Konfigurationsdatei bereitgstellt wurde und das aktuelle Datum zurückgibt

## Server-Proxy
Standardmäßig agieren alle Nicht-Masterserver als Proxies. Dies bedeutet, dass **Job-Kommandos** welche an diesen ankommen 
nicht auf dem Server selbst ausgeführt werden, sondern an den Master-Server weitergeleitet werden, dort ausgeführt 
werden und das Ergebnis dann über den Proxy-Server und die eigentliche Verbindung an den Client zurück gesendet wird.
Um dieses Verhalten in einem Server zu deaktivieren, kann eine Flag gesetzt werden, welche dem Server mitteilt, 
dass Job-Kommandos immer lokal auszuführen sind. Z.B. über die Kommandozeile mithilfe der Flag `--disable-proxy`:

```shell
python3 start-server.py --host=127.0.0.1 --port=5003 --config=server.config.yaml --disable-proxy
```

Führt man nun den Client gegen diesen Server aus und ruft z.B. das Kommando `job:get-date` auf, sieht man, dass dieses
Kommando nicht mehr an den Master-Server weitergeieltet wird, sondern lokal auf dem 3. Server ausgeführt wird:

```shell
Ready to send commands to 127.0.0.1 on 5003 | Enter `exit` to abort.
=> job:get-time
19:29:37

=>
```

## Testing
Die Tests gegen die Standardkonfiguration können wie folgt ausgeführt werden:
```bash
 python3 test-server.py
```