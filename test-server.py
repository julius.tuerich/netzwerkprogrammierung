import unittest
from network.server import Server
from network.server_node import ServerNode


class TestServer(unittest.TestCase):

    def test_Server(self):
        server = Server("127.0.0.1", 5001, "server.config.yaml", False)

        self.assertEqual(
            server.host,
            "127.0.0.1",
            "Host should be 127.0.0.1")
        self.assertEqual(
            server.port,
            5001,
            "Port should be 5001")
        self.assertEqual(
            server.config_file,
            "server.config.yaml",
            "Config file should be 'server.config.yaml'")
        self.assertEqual(
            server.proxy_to_master,
            False,
            "Proxy to master should be false")

    def test_ServerNode(self):
        server_node = ServerNode("Server-One", "127.0.0.1", 5001)

        self.assertEqual(
            server_node.name,
            "Server-One",
            "Name should be 5001")
        self.assertEqual(
            server_node.host,
            "127.0.0.1",
            "Host should be 127.0.0.1")
        self.assertEqual(
            server_node.port,
            5001,
            "Port should be 5001")
        self.assertEqual(
            server_node.index(),
            21307064335001,
            "Index should should be 21307064335001")
        self.assertEqual(
            ServerNode("Server-One", "127.0.0.1", 5003),
            ServerNode("Server-Two", "127.0.0.1", 5003),
            "Index should should be 21307064335001")

    def test_ServerHandle(self):
        server = Server("127.0.0.1", 5001, "server.config.yaml", False)

        self.assertEqual(
            server.handle("UNIT-TEST", "server:status"),
            "OK! 200",
            "Command should be correct")
        self.assertEqual(
            server.handle("UNIT-TEST", "server:info"),
            "TCP server running at 127.0.0.1 on 5001",
            "Command should be correct")
        self.assertEqual(
            server.handle("UNIT-TEST", "server:exit"),
            "SUCCESS!",
            "Command should be correct")
        self.assertEqual(
            server.handle("UNIT-TEST", "server:ping"),
            "Ping OK!",
            "Command should be correct")

    def test_ServerGetServerNodes(self):
        server = Server("127.0.0.1", 5001, "server.config.yaml", False)

        self.assertEqual(
            len(server.get_server_nodes()),
            3,
            "Should be three servers in configuration file by default")

    def test_ServerLoadConfig(self):
        server = Server("127.0.0.1", 5001, "server.config.yaml", False)

        self.assertTrue(
            isinstance(server.load_config(), dict),
            "Should be instance of type dict")


if __name__ == '__main__':
    unittest.main()
